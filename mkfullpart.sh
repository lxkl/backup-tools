#!/bin/bash
set -e
raw_device_="${2?}"
parted "${raw_device_?}" mklabel gpt
case "${1?}" in
    normal) parted -a optimal "${raw_device_?}" mkpart primary 0% 100% ;;
    raid)   parted -a optimal "${raw_device_?}" mkpart primary 0%  99%
            parted "${raw_device_?}" set 1 raid on
            ;;
esac
echo "All done!"
