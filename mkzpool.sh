#!/bin/bash
set -e
dev_="${1?}"
pool_="${2?}"
dataset_="${3?}"
zpool create -f -o ashift=12 -m none "${pool_?}" "${dev_?}"
zfs create -o mountpoint=none -o encryption=on -o keyformat=passphrase "${pool_?}"/"${dataset_?}"
zfs set compression=lz4 "${pool_?}"/"${dataset_?}"
echo "All done!"
