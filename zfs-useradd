#!/usr/bin/env ruby
$VERBOSE = true
require_relative "../misclib/ruby/system.rb"
require_relative "../misclib/ruby/zfs.rb"
require "fileutils"
require "optparse"
USAGE_MESSAGE = "usage: zfs-useradd username dataset mountpoint"
def die_usage
  puts USAGE_MESSAGE
  exit 100
end
die_usage unless ARGV.length == 3
OptionParser.new do |opts|
  opts.program_name = "zfs-useradd"
  opts.version = "0.0.1"
  opts.banner = USAGE_MESSAGE
end.parse!
username = ARGV[0]
dataset = ARGV[1]
mountpoint = ARGV[2]
FileUtils.mkdir_p(File.dirname(mountpoint))
system_ok("zfs", "create", dataset)
system_ok("zfs", "set", "mountpoint=#{mountpoint}", dataset)
zfs_remount(dataset)
unless zfs_mounted_at?(dataset, mountpoint)
  puts "fatal: dataset not mounted"
  exit 111
end
useradd_cmd = ["useradd", username, "-m", "-d", mountpoint]
case capture("uname", "-s")
when "FreeBSD" then system_ok("pw", *useradd_cmd)
when "Linux" then system_ok(*useradd_cmd, "-U")
else puts "fatal: unsupported operating system"; exit 111
end
FileUtils.chmod(0700, mountpoint)
FileUtils.chown(username, username, mountpoint)
puts "info: added user with own ZFS dataset"
