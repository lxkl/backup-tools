#!/bin/bash
set -e
sedfile_() {
    sed "${1?}" "${2?}" > tmp.sed
    cp tmp.sed "${2?}"
    rm tmp.sed
}
sedfile_ 's%\s*#\s*\(auth\s\+required\s\+pam_wheel\.so\)\s*$%\1%' /etc/pam.d/su
sedfile_ 's%^[[:space:]#]*\(PermitRootLogin\|PasswordAuthentication\|PermitEmptyPasswords\)[[:space:]].*%\1 no%' /etc/ssh/sshd_config
sedfile_ 's%^deb cdrom:.*%# \0%' /etc/apt/sources.list
sedfile_ 's% main.*% main contrib non-free%' /etc/apt/sources.list
apt-get update
apt-get -y dist-upgrade
apt-get -y install gauche                                     # programming language that i use
apt-get -y install git bup xz-utils pax rsync magic-wormhole  # file tools
apt-get -y install tree hashdeep                              # more file tools
apt-get -y install mdadm parted xfsprogs jfsutils btrfs-progs # partition and filesystems tools
apt-get -y install cryptsetup                                 # more partition and filesystems tools
apt-get -y install linux-headers-"`uname -r`" zfs-dkms        # zfs
apt-get -y install net-tools htop hddtemp atop glances        # system monitoring
apt-get -y install postfix mutt s-nail                        # e-mail
apt-get -y install sudo emacs-nox                             # misc
cp cron.d/apt-get-update cron.d/apt-get-dist-upgrade cron.d/plugbak /etc/cron.d
ln -sf "`pwd`"/prot-bup-init "`pwd`"/prot-bup-protect "`pwd`"/prot-bup-fixperms /usr/local/sbin
ln -sf "`pwd`"/btrcrypt-* "`pwd`"/mkcrypt /usr/local/sbin
ln -sf "`pwd`"/prot-bup-aftersave "`pwd`"/prot-bup-test-aftersave "`pwd`"/plugbak-* /usr/local/bin
echo "Prepared this system for protected bup successfully."
