#!/bin/bash
set -e
progname_=btrcrypt-format
if test "$#" -lt 2; then
    echo "usage: ${progname_?} passphrase device [device ...]"
    exit 10
fi
for file_ in config-fstype config-mkfs-opts; do
    if test ! -f "${file_?}"; then
        echo "${progname_?}: fatal: no such file: ${file_?}"
        exit 11
    fi
done
for file_ in config-uuids; do
    if test -e "${file_?}"; then
        echo "${progname_?}: fatal: file exists: ${file_?}"
        exit 11
    fi
done
if test ! -e config-key; then
    dd if=/dev/urandom of=config-key bs=1024 count=4
fi
passphrase_="${1?}"
shift
for raw_device_; do
    if test ! -b "${raw_device_?}"; then
        echo "${progname_?}: fatal: not a block device: ${raw_device_?}"
        exit 11
    fi
    dd if=/dev/zero of="${raw_device_?}" bs=1M count=100
done
for raw_device_; do
    parted "${raw_device_?}" mklabel gpt
    parted -a optimal "${raw_device_?}" mkpart primary 0% 99%
    sleep 5
    partprobe "${raw_device_?}"
    sleep 5
    device_="${raw_device_?}"1
    echo -n "${passphrase_?}" | cryptsetup -q luksFormat "${device_?}"
    echo -n "${passphrase_?}" | cryptsetup --key-file - luksAddKey "${device_?}" config-key
    cryptsetup luksUUID "${device_?}" >> config-uuids
done
btrcrypt-unlock
fstype_="`cat config-fstype`"
mkfs_opts_="`cat config-mkfs-opts`"
device_args_=
for uuid_ in `cat config-uuids`; do
    device_args_="${device_args_?} /dev/mapper/btrcrypt-${uuid_?}"
done
mkfs."${fstype_?}" ${mkfs_opts_?} ${device_args_?}
echo "${progname_?}: successully formatted"
