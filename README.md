Backup Tools
============

Copyright 2017 Lasse Kliemann <lasse@lassekliemann.de>

License: GPLv3 <https://www.gnu.org/licenses/gpl.html>

Status: *pre-alpha*

Programming languages: Bash and [Gauche](http://practical-scheme.net/gauche/)

Protected Bup
-------------

Read [about Bup here](https://bup.github.io/).
The Protected Bup system implements trojan-proof backups based on Bup.
It is mainly a howto with a couple of short shell scripts.

If your computer has been compromised,
any backup that is write-accessible from it can be compromised too.
This project proposes the following scheme:

* Set up a dedicated hardware that will only hold the backups.
  For home use, a decent second-hand laptop with sufficient disk space will probably be fine.
  Call this the _backup machine_.
  There should be no remote root access to this machine.
  
* Backups are copied from the workstation to the backup machine over SSH with `bup save`.
  For SSH access, a normal user account on the backup machine is used,
  call this account `bak`.
  
* Periodically, a cron job on the backup machine protects the backup files
  by chowning them to root and making them non-writeable to `bak`.

One of the great features of Bup is that most files do not need to be changed
after being written once.
The few files that need changing over time amount to a few kilobyte
and are protected separately (in the `meta` dir, see below).

A script `init-debian.sh` is provided that can be used to make
a fresh install of Debian ready for use as an operating system on the backup machine.
It does a bit of hardening stuff,
installs packages that can be helpful when dealing with files in general,
installs cron files in `/etc/cron.d`,
and links the commands from Backup Tools into `/usr/local/sbin` and `/usr/local/bin`.
The cron files for `apt-get update` and `apt-get dist-upgrade` are so that
about a month passes between the two,
that is, the `dist-update` happens based on package data that is a month old.
The idea is that if the Debian package repositories should ever be compromised,
it is likely that I will learn about it within a month's time,
so I can disable the upgrade and my backup machine will not be compromised.
When I install Debian for this task,
I choose only "SSH server" and "standard system utilities" in the installer.
Please adjust to your needs.

Operation of Protected Bup will be explained in the following
by explaining the different commands.
A few remarks on restoring come after that.
Please be aware that when recovering from a break-in, many things have to be considered,
far beyond the scope of this document.

### `prot-bup-init account`

This should be called on the backup machine to set up the account
which will be used to connect to the machine via SSH
in order to deliver the backups.
This account is called `bak` here
and it must exist before calling `prot-bup-init`.
Example:

    useradd -m -s /bin/bash bak
    prot-bup-init bak

A crontab will be installed in `/etc/cron.d` to run `prot-bup-protect` once every hour for this account.

Multiple such accounts can be hosted on the same backup machine.

### `prot-bup-protect account`

This should be run on the backup machine periodically
in order to protect newly arrived backup files.
You normally do not call this command manually but let a cron job do it (see above).

### `prot-bup-aftersave`

In order that `prot-bup-protect` knows which files have been added,
run this on the backup machine under the account `bak` without any arguments.
It will write a file in the directory `packlist` that lists the files in the Bup repository
which have been added.
It also makes a backup of those files in the repository that have to stay writeable by `bak`.
This backup is done in form of a tar file in directory `meta`
and will be protected by the next run of `prot-bup-protect`.

### `prot-bup-save config-file`

Not much more than a wrapper around `bup-save` that calls `prot-bup-aftersave` before concluding.
You will probably want to write your own,
or extend [bup-cron](https://github.com/anarcat/bup-cron).
Here is the structure of a config file, the parts in caps have to be replaced:

    ((name NAME)
     (dir DIR)
     (exclude PATH ...)
     (index-extra-args ARGS ...)
     (save-extra-args ARGS ...)
     (remote SSH-URL))
    ...

For example:

    ((name "home")
     (dir "/home/snyder")
	 (index-extra-args "--xdev")
	 (remote "bak@192.168.2.12"))
    ((name "Music")
     (dir "/media/snyder/external/Music")
	 (index-extra-args "--xdev")
	 (remote "bak@192.168.2.12"))
    ((name "Videos")
     (dir "/media/snyder/external/Videos")
	 (index-extra-args "--xdev")
	 (remote "bak@192.168.2.12"))
    ((name "lite")
     (dir  "/home/snyder")
     (exclude "Music/" "Videos/")
   	 (index-extra-args "--xdev")
     (remote "baklite@192.168.2.12"))

The excludes are appended to `dir`.
A separate `BUP_DIR` is used for each of the records (four in this example).
Path stripping is done automatically so that only the basename of the `dir` will be saved.
When indexing and saving is done, `prot-bup-aftersave` is called on each remote once.

### `prot-bup-fixperms account`

Normally, permissions on the account `bak` will be kept in order automatically.
But maybe some day, you run something like `bup gc` on the repository as root on the backup machine.
Then the permissions may become messed up.
Correct by running as root:

    prot-bup-fixperms bak
	
Such should only be done while no new backup files are delivered.

### How to restore

Log into the backup machine as `bak` and use `bup restore`.
Then use `rsync` to copy the restored files where you need them.

If indeed the `bak` account had been compromised,
you may have to clean up first.
Any file that was not protected before the break-in cannot be trusted.
Backups of files that cannot be protected in the repository
can be found tars in `meta`.
Those backup tar files have timestamps in their names, but those cannot be trusted.
Rather look at their ctimes.
The ctimes give a reasonable accurate account of when the file was protected
(note however that `prot-bup-fixperms` can destroy this information).

*TODO:* a set of forensic tools would be nice.

Here is an example how to list the files in `meta` by increasing ctime
with a human-readble version of the ctime included:

    find meta -printf '%C@ %c %p\n' | sort

Backups with pluggable media
----------------------------

The Plugbak system is for backups on pluggable media, such as USB sticks and hard drives.
The backend is `rsync`.
A configurable number of backups can be kept,
and `--link-dest` is used in order to save space.
Information for each medium is stored in a separate directory (a _config dir_),
and configuration is mostly via files in such directories.
Two short scripts are provided to initialize a medium:

### `plugbak-init [fstype]`

Populate the current directory with configuration files.
The filesystem type to be used for the medium (see below)
can be given as an optional argument.
If none is given, `ext4` is used.
Example:

    mkdir medium-1
    cd medium-1
    plugbak-init btrfs

### `plugbak-format passphrase device`

Format the given device for use with the Plugbak system
based on information found in the current directory,
which usually has earlier been initialized using `plugbak-init`.
The filesystem on the device will be encrypted, of course.
The passphrase given on the command line will be configured.
In addition, a key is stored in the config dir (`config-key`),
which allows unattended operation.
Example:

    plugbak-format correct.horse.battery.staple /dev/sdb2

All other operation is through a program `plugbak-do`.
Before we get to it, we explain the different files in a config dir:

### Files in a config dir

#### `config-ignore`

If this file exists, this config dir is ignored by the two commands for doing backups
(i.e., `plugbak-do cond` and `plugbak-do now`, see below).
It is created first thing by `plugbak-init`.
This mechanism is helpful if there is a cron job that can start a backup anytime on this config dir.
When all is configured, then delete this file.

#### `config-fstype` and `config-mount-opts`

Filesystem type and mount options.
Reasonable defaults are provided by `plugbak-init`.

#### `config-uuid`

A UUID by which `plugbak-do` can recognize the medium
and in particular determine whether it is plugged in or not.
It is created by `plugbak-format`.

#### `config-key`

Key to unlock the encrypted filesystem.
It is created by `plugbak-format`.

#### `config-source-dir`

The directory which is to be backed up, best given as an absolute path.
*This option must be configured manually.*
Since `rsync` is used for transfer,
a trailing slash is significant (see `rsync` manpage).

#### `config-btrfs-source` and `config-btrfs-snap`

If the files to be backed up are on a btrfs subvolume,
these options should be used in order that backup is conducted from a snapshot
instead of from a live filesystem.
By `config-btrfs-source`, the subvolume with the files to be backed up is designated.
A snapshot of this subvolume will be created under the name given in `config-btrfs-snap`
before the backup process.
The snapshot subvolume (that is, the one named in `config-btrfs-snap`)
will be deleted by `plugbak-do` when no longer required
and it will also be deleted (if existing) before a new snapshot is taken,
in case an old snapshot is still there from a previous run.

If `config-btrfs-snap` is not a prefix of `config-source-dir`,
then most probably you are doing something wrong.

Examples:

`config-btrfs-source`:
    /data/main

`config-btrfs-snap`:
    /data/plugbak-snap-tmp

`config-source-dir`:
    /data/plugbak-snap-tmp/photos/

#### `config-zfs-source` and `config-zfs-snap`

Similar for ZFS, but the semantics are a bit different.
The `config-zfs-source` is the name of the source volume
and `config-zfs-snap` is the name of the snapshot that will be created relatively to it.
The snapshot will be visible in a special directory `.zfs` wherever the volume is mounted,
so `config-source-dir` should be set accordingly.

Examples:

`config-zfs-source`:
    data/main

`config-zfs-snap`:
    plugbak-snap-tmp

`config-source-dir`:
    /data/main/.zfs/snapshot/plugbak-snap-tmp/photos/

#### `config-dest-subdir`

Subdirectory on the medium where the backups will be stored.
Is set by `plugbak-init` and defaults to `backup`.

#### `config-name`

A human readable name for this medium.
This name is used for informational output by `plugbak-do`.
The name is set to the basename of the config dir by `plugbak-init`.

#### `config-mountpoint`

Where the medium will be mounted during operation.
This config file is not there by default,
in which case the mountpoint will be `mnt` in the config dir.

#### `config-retention`

This many previous backups are kept.
Default `10` is provided by `plugbak-init`.

#### `config-interval`

Minimum time between two backups in seconds.
If `plugbak-do cond` is run (see below), it will only do a backup if this amount of time
has elapsed since the last backup.
A file `run-mark` is used to record the last run.
The default is 14400 seconds, which is 4 hours.

#### `config-include*`, `config-exclude*`, and `config-filter*`

All files matching those patterns are concatenated into
files `run-include`, `run-exclude`, and `run-filter`, respectively, before a backup,
and then these files are announced to `rsync`.
This can be used to define inclusion and exclusion rules.
For example, this could be used as a `config-exclude`:

    /mail/.notmuch/
    /tmp/
    /temp/

This will exclude the listed directories under the directory to backup,
which is given in `config-source-dir`.

#### `config-diff`

If this file exists and contains a number,
then it is checked if the current run number modulo the number in this file is zero.
If this is the case, then files on the source and the backup will be compared after copying.
The results of the comparison is written to files ending in `diff.1` and `diff.2`
(for stdout and stderr from the diff command) on the backup medium.
Those two files are then also output to stdout for logging purposes.
The run number is kept track of via the file `run-count`.

Currently, a simple `diff -ru` is used, but more elaborate solutions should be implemented,
in particular, metadata should also be compared.
The `star` programm ([see here](http://freshcode.club/projects/schily)) was perfect for this,
but it is not available on Debian.
Any suggestions?
The solution should not be based on `rsync` since in particular,
this step is intended to detect errors in `rsync`'s operation.

### `plugbak-do command config-dir [config-dir ...]`

This program executes the given command for each of the config dirs.
Commands are:

#### `unlock`

Unlock the encrypted filesystem on the medium if the medium is connected to the machine.
A device will appear in `/dev/mapper` with the name of the UUID.

#### `mount`

Mount the previously unlocked medium at the mountpoint
given by `config-mountpoint`
or at `mnt` in the config dir.

#### `open`

Unlock and then mount.

#### `close`

Unmount (if mounted) and then lock the encrypted filesystem.

#### `list`

Give a list of available backups with an indication whether they are complete
or partial (resulting from an interrupted operation).
The medium must be opened for this to work.

#### `cond` and `now`

Do a backup.
The first is a conditional backup,
meaning it will only be run if enough time has elapsed sind the previous backup,
see `config-interval` and `run-mark` above.
Just as with `unlock`, if the medium is not connected,
nothing bad will happen apart from a couple of error messages.
With `now`, the backup is run unconditionally.

If a previous backup exists, `--link-dest` is used to refer to it in order to save space.
A list of the files that were actually transferred is stored with the backup on the medium.

### Regular operation

A typical setup would be a directory with a collection of config dirs in it,
each corresponding to a medium:

    /home/snyder/plugbak-media/home-1
    /home/snyder/plugbak-media/home-2
    /home/snyder/plugbak-media/work-1
    /home/snyder/plugbak-media/suitcase

A cron job is used to run

    plugbak-do cond /home/snyder/plugbak-media/*

every minute or so.
This way, a medium will be detected within this time frame and a backup will be done
conditional on the time that has elapsed since the last backup.
The option `-q` is usefull here in order to suppress the error message
that the lock cannot be obtained,
which would otherwise produce a lot of messages when a backup run takes longer.
A cron line could look like:

    * * * * * plugbak-do -q cond ~/plugbak-media/*

### Restoring

Open the medium with `plugbak-do open` and copy the files from it as needed.
If the config dir is no longer available, do for example:

	sudo cryptsetup luksOpen /dev/sdb backup
	sudo mount -o ro /dev/mapper/backup /mnt

Or use the encrypted filesystem handling of your desktop environment.
Instead of `config-key` (which may be lost at this time),
the password set during `plugbak-format` is now required.

### Robustness

When using the `ext4` filesystem, the medium can be disconnected at any time.
If in the middle of a backup when the interruption occurs, `plugbak-do` will continue later.
All the other filesystems that I tested around 2015 were not this forgiving.
The most common problem is a kind of "half-mounted" filesystem which requires a reboot.

Combining both systems
----------------------

Probably the best idea is having Plugbak on the Protected Bup backup machine
with multiple media that are rotated according to a certain scheme.
For example, take 3 media.
There is always one that is connected to the machine,
one that is nearby but disconnected,
and one at remote place (different building).
In certain intervals, say of T days,
the connected and nearby disconnected ones are swapped
and then the newly disconnected one is swapped with the remote one.
If the place with the backup machine should be destroyed,
the work of at most T days is lost.

A note on filesystems
---------------------

At the time of writing (October 2017), btrfs in my experience is way too unstable for production.
In one incident, it filled up my logs with gigabytes of warning or error messages.
It also appeared as if when rsyncing from a btrfs snapshot,
the `link-dest` function sometimes does not work properly.
I had [something similar](https://serverfault.com/q/711364/264530) before,
but that was with a different filesystem.

For the Protected Bup backup machine, I'm currently trying zfs.
It works well so far, but it is too early to give a recommendation.

On pluggable media, I have great experience with ext4.
